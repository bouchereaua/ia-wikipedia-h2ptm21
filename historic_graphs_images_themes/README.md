# Evolution du graphe de liens de 2003 à 2021
Les images PNG ci-après sont les versions annotées des graphes générés à partir de Gephi pour chaque année, de 2003 à 2021. Les images PNG sans annotations sont disponibles dans le dossier "images_brutes".

## 2003

![2003](../historic_graphs_images_themes/images_annotees/2003_labels.jpg)

## 2004

![2004](../historic_graphs_images_themes/images_annotees/2004_labels.jpg)

## 2005

![2005](../historic_graphs_images_themes/images_annotees/2005_labels.jpg)

## 2006

![2006](../historic_graphs_images_themes/images_annotees/2006_labels.jpg)

## 2007

![2007](../historic_graphs_images_themes/images_annotees/2007_labels.jpg)

## 2008

![2008](../historic_graphs_images_themes/images_annotees/2008_labels.jpg)

## 2009

![2009](../historic_graphs_images_themes/images_annotees/2009_labels.jpg)

## 2010

![2010](../historic_graphs_images_themes/images_annotees/2010_labels.jpg)

## 2011

![2011](../historic_graphs_images_themes/images_annotees/2011_labels.jpg)

## 2012

![2012](../historic_graphs_images_themes/images_annotees/2012_labels.jpg)

## 2013

![2013](../historic_graphs_images_themes/images_annotees/2013_labels.jpg)

## 2014

![2014](../historic_graphs_images_themes/images_annotees/2014_labels.jpg)

## 2015

![2015](../historic_graphs_images_themes/images_annotees/2015_labels.jpg)

## 2016

![2016](../historic_graphs_images_themes/images_annotees/2016_labels.jpg)

## 2017

![2017](../historic_graphs_images_themes/images_annotees/2017_labels.jpg)

## 2018

![2018](../historic_graphs_images_themes/images_annotees/2018_labels.jpg)

## 2019

![2019](../historic_graphs_images_themes/images_annotees/2019_labels.jpg)

## 2020

![2020](../historic_graphs_images_themes/images_annotees/2020_labels.jpg)

## 2021

![2021](../historic_graphs_images_themes/images_annotees/2021_labels.jpg)


