# Évolution des représentations de l’intelligence artificielle
# De quelles(s) IA Wikipédia est-elle l’encyclopédie ?

Ce dépôt contient les données d'une étude analysant le rôle de Wikipédia dans la diffusion et la circulation des savoirs relatifs à l'Intelligence artificielle (IA).

Ce travail est effectué par Aymeric Bouchereau ([bouchereaua.net](https://bouchereaua.net/)) et Antonin Segault ([retrodev.net](https://retrodev.net/)) et fait l'objet d'un article scientifique présenté lors de la 16ème édition de la [conférence internationale H2PTM](http://h2ptm.univ-paris8.fr/) en octobre 2021. [Consulter les slides de la communication](https://framagit.org/bouchereaua/ia-wikipedia-h2ptm21/-/blob/master/slides_communication_h2ptm21.pdf) 

## Données brutes
Le dossier "historic_graphs_donnees_brutes" contient les données brutes collectées avec l'aide de [Historic Graphs (Segault, 2019)](https://framagit.org/retrodev/historic-graphs/-/tree/master/). Il s'agit de 19 fichiers CSV comportant les liens hypertextes entourant l'article "Intelligence artificielle" de Wikipédia en date des premiers janvier de 2003 à 2021.

Pour plus de détails sur la lecture des fichiers, voir le [tutoriel de Historic Graphs](https://framagit.org/retrodev/historic-graphs/-/tree/master/examples/tutorial).

## Evolution du graphe de liens hypertextes
Le [dossier "historic_graphs_images_themes" contient les 19 graphes](../historic_graphs_images_themes/README.md) (pour chaque année) générées à partir des données brutes avec le logiciel Gephi et après application de l'algorithme de spatialisation Force Atlas 2. Ajouté à la spatialisation, le fichier "themes_noeuds_graphe.csv" rapporte les thématiques qui ont été associées à chaque noeud du graphe.

## Publications scientifiques et articles de presse
Le dossier "donnees_europresse_dimensions" contient les données collectées auprès des bases de données [Dimensions](https://app.dimensions.ai/discover/publication) et [Europresse](http://www.europresse.com/fr/). Les données correspondent aux nombres de publications scientifiques et d'articles de presse de 2003 à 2020 citant l'un des mots-clés suivants : « AlphaGo », « DeepMind », « IBM&Watson », « ‘deep learning’ », « superintelligence », « ‘singularité technologique’ ».

## Auteurs

Aymeric BOUCHEREAU
aymeric.bouchereau@u-pec.fr
Laboratoire Lab'Urba
Université Paris-Est Créteil

Antonin SEGAULT
antonin.segault@parisnanterre.fr
Laboratoire Dicen-IDF
Université Paris Nanterre